import tensorflow as tf
import os

# ignore warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# create a constant
hw = tf.constant("hello world!")

# create a session
sess = tf.Session()


print(sess.run(hw))

# close session
sess.close()